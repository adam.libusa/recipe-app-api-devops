resource "aws_s3_bucket" "app_public_files" {
  bucket = "${local.prefix}-files-adam-libusa" // the bucket namespace is shared by all users of the system

  // The following line (private instead of public-read)
  // will cause a 500 during a file upload, but we would have to update Terraform to fix it
  acl           = "private"
  force_destroy = true
}